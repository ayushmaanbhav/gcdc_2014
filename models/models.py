from google.appengine.ext import db


class User(db.Model):
    user_id = db.StringProperty(required=True)
    email = db.EmailProperty(required=True, validator=None)
    username = db.StringProperty(required=True)
    name = db.StringProperty(required=True)
    phone = db.StringProperty(required=True)
    country = db.StringProperty(required=True)
    state = db.StringProperty()
    city = db.StringProperty(required=True)
    pincode = db.IntegerProperty(required=True)
    address = db.StringProperty()
    about = db.TextProperty()
    motto = db.StringProperty()
    qualifications = db.StringProperty(required=True)
    profilepic = db.StringProperty()


class Category(db.Model):
    category_id = db.StringProperty(required=True)
    category_name = db.StringProperty(required=True)
    description = db.TextProperty(required=True)


class Course(db.Model):
    course_title = db.StringProperty(required=True)
    category = db.ReferenceProperty(Category)
    desc = db.TextProperty()
    #TODO make validator
    #gdrive = db.LinkProperty()
    #TODO make validator
    forum = db.LinkProperty()


#subcategory in course
class Topic(db.Model):
    title = db.StringProperty()
    desc = db.TextProperty()
    course = db.ReferenceProperty(Course)
    #user can enter individual videos (already implemented) or a playlist
    links = db.ListProperty(db.Link)


class CourseTaken(db.Model):
    user = db.ReferenceProperty(User)
    course = db.ReferenceProperty(Course)
    completed = db.IntegerProperty()
    performance = db.IntegerProperty()


class CourseTaught(db.Model):
    user = db.ReferenceProperty(User)
    course = db.ReferenceProperty(Course)
    #level of admin i.e. admin=0, manager=1, etc.
    level = db.IntegerProperty()


#a live class via hangouts after it happens its link will be added to the topic links
class Class(db.Model):
    course = db.ReferenceProperty(Course)
    topic = db.ReferenceProperty(Topic)
    title = db.StringProperty()
    link = db.LinkProperty()
    start = db.DateTimeProperty()
    #we don't require end time
    #end = db.DateTimeProperty()


class CourseFeedBack(db.Model):
    user = db.ReferenceProperty(User)
    course = db.ReferenceProperty(Course)
    feedback = db.TextProperty()


class ClassTaken(db.Model):
    class_key = db.ReferenceProperty(Class)
    user = db.ReferenceProperty(User)


class CourseLikes(db.Model):
    course = db.ReferenceProperty(Course)
    user = db.ReferenceProperty(User)
