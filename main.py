import webapp2
from controllers import userController
#from controllers import classController
#from controllers import newclassController
from controllers import generate
from controllers import courseController
from controllers import searchController


login = webapp2.WSGIApplication([('/', userController.startPageController),
                                 ('/login', userController.startPageController),
                                 ('/google/login', userController.googleLoginController),
                                 ('/login/check', userController.loginRedirectStation),
                                 ('/login/create', userController.CreateUserController),
                                 ('/login/create/submit', userController.CreateUserSubmitForm),
                                 ('/profile', userController.showProfile),
                                 ('/profile/mycourses', userController.myCourses),
                                 ('/profile/myclasses', userController.myClasses),
                                 ('/contactus', generate.ContactUs),
                                 ('/profile/search', userController.search),
                                 #('/class/view', classController.viewClasses),
                                 #('/class/create', classController.createClass),
                                 #('/class/update', classController.updateClass),
                                 #('/class/details', classController.viewDetails),
                                 #('/class/delete', classController.deleteClass),
                                 #('/class/session/create', classController.createSession),
                                 #('/class/session/update', classController.updateSession),
                                 #('/class/session/details', classController.viewSession),
                                 #('/class/session/delete', classController.deleteSession),
                                 ('/generate', generate.GenerateTestData),
                                 ('/picker', generate.pickerHandler),
                                 ('/hangout', generate.hangoutHandler),
                                 ('/hangout/start', generate.hangoutStart),
                                 ('/course/create', courseController.showCourseCreate),
                                 ('/course/create/submit', courseController.createCourse),
                                 ('/course/update', courseController.showCourseUpdate),
                                 ('/course/update/submit', courseController.updateCourse),
                                 ('/course/links', courseController.showAddLinksPage),
                                 ('/course/links/add', courseController.addLink),
                                 ('/course/links/delete', courseController.deleteLink),
                                 ('/course/finish', courseController.courseAdded),
                                 ('/course/enroll', courseController.enrollInCourse),
                                 ('/course/leave', courseController.leaveCourse),
                                 ('/course/like', courseController.likeCourse),
                                 ('/course/unlike', courseController.unlikeCourse),
                                 ('/course/feedback/submit', courseController.submitFeedback),
                                 ('/course/goto', courseController.viewCourse),
                                 ('/search', searchController.searchController)
                                ],
                                debug=True)

app = login