__author__ = 'arun'

import webapp2
from google.appengine.api import users
from models.models import Category, ClassGroup, ClassSession
import os
from google.appengine.ext.webapp import template
from datetime import datetime
from google.appengine.ext import db


class createClass(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect("/login")
        else:
            categories = Category.all()

            path = os.path.join(os.path.dirname(__file__), '../views', 'createClassGr.html')
            self.response.write(template.render(path, {'categories': categories}))

    def post(self):

        title = None
        desc = None
        category = None
        c_err = 0
        err = ""

        try:
            title = self.request.get("title")
        except:
            c_err += 1
            err += "Invalid title"

        try:
            desc = self.request.get("desc")
        except:
            c_err += 1
            err += "Invalid Description"

        try:
            category = self.request.get("category")
        except:
            c_err += 1
            err += "Invalid Category"

        if c_err == 0:

            classGroup = ClassGroup()
            classGroup.title = title
            classGroup.description = desc
            classGroup.category = Category.get(category)

            classGroup.put()
            self.redirect("/class/view")

        else:
            self.redirect_with_errors(err)

    def redirect_with_errors(self, err):
        categories = Category.all()

        path = os.path.join(os.path.dirname(__file__), '../views', 'createClassGr.html')
        self.response.out.write(template.render(path, {'categories': categories, 'err': err}))


class viewClasses(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect("/login")
        else:
            classes = ClassGroup.all()
            path = os.path.join(os.path.dirname(__file__), '../views', 'viewClasses.html')
            self.response.out.write(template.render(path, {'classes': classes}))


class viewDetails(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect("/login")
        else:
            classGroup = ClassGroup.get(self.request.get("key"))
            sessions = ClassSession.get(classGroup.classes)
            path = os.path.join(os.path.dirname(__file__), '../views', 'classDetails.html')
            self.response.out.write(template.render(path, {'class': classGroup, 'sessions': sessions}))


class updateClass(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect("/login")
        else:
            classGroup = ClassGroup.get(self.request.get("key"))
            categories = Category.all()
            path = os.path.join(os.path.dirname(__file__), '../views', 'updateClass.html')
            self.response.out.write(template.render(path, {'class': classGroup, 'categories': categories}))

    def post(self):
        title = None
        desc = None
        category = None
        key = None
        c_err = 0
        err = ""

        try:
            title = self.request.get("title")
        except:
            c_err += 1
            err += "Invalid title"

        try:
            desc = self.request.get("desc")
        except:
            c_err += 1
            err += "Invalid Description"

        try:
            category = self.request.get("category")
        except:
            c_err += 1
            err += "Invalid Category"

        try:
            key = self.request.get("key")
        except:
            c_err += 1
            err += "Invalid Key"

        if c_err == 0:

            classGroup = ClassGroup.get(key)
            classGroup.title = title
            classGroup.description = desc
            classGroup.category = Category.get(category)

            classGroup.put()
            self.redirect("/class/view")

        else:
            self.redirect_with_errors(err)

    def redirect_with_errors(self, err):
        categories = Category.all()

        path = os.path.join(os.path.dirname(__file__), '../views', 'updateClass.html')
        self.response.out.write(template.render(path, {'categories': categories, 'err': err}))


class deleteClass(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect("/login")
        else:
            classGroup = ClassGroup.get(self.request.get("key"))
            classGroup.delete()
            self.redirect("/class/view")


class createSession(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
        else:
            path = os.path.join(os.path.dirname(__file__), '../views', 'createClass.html')
            self.response.out.write(template.render(path, {'key': self.request.get("key")}))

    def post(self):
        """
        Checks inputs and creates a new class table
        """
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
        else:
            #try and add the class to the database else show
            #where the error has occurred
            startDate = None
            endDate = None
            startTime = None
            endTime = None

            # classid=self.request.get('cname')

            err = ""
            c_err = 0

            #check if unique id is present or not
            # if not self.checkUniqueClassID(classid):
            #     err+="\nClass Name already present.It has to be unique"
            #     c_err+=1

            #check if date is parsible or not
            try:
                # print self.request.get('sdate')
                print datetime.strptime(self.request.get('sdate'), '%d-%m-%Y')

                print datetime.strptime(self.request.get('sdate'), '%d-%m-%Y').date()
                startDate = datetime.strptime(self.request.get('sdate'), '%d-%m-%Y').date()
            except Exception:
                err += "\nEnter a valid start date.Date should be ahead of today"
                c_err += 1

            #check if date is parsible or not
            try:
                endDate = datetime.strptime(self.request.get('edate'), '%d-%m-%Y').date()
            except:
                err += "\nEnter a valid end date.End date should be ahead of start date"
                c_err += 1

            #check if time is parsible or not
            try:
                startTime = datetime.strptime(self.request.get('sHrs'), '%H:%M').time()
            except:
                err += "\nEnter a valid time in startTime.Format 24Hrs"

            #check if end time is parsible or not
            try:
                endTime = datetime.strptime(self.request.get('eHrs'), '%H:%M').time()
            except:
                err += "\nEnter a valid ending time.Time format is 24Hrs"
                c_err += 1

            if c_err == 0:
            #check if can be put in database

                cl = ClassSession(title=self.request.get('title'))
                cl.start_date = startDate
                cl.end_date = endDate
                cl.start_time = startTime
                cl.end_time = endTime
                cl.put()
                classGroup = ClassGroup.get(self.request.get("key"))
                classGroup.classes.append(cl.key())
                classGroup.put()
                self.redirect("/class/details?key=" + self.request.get("key"))
            else:
                self.redirect_with_errors(err)

    def redirect_with_errors(self, err):
        path = os.path.join(os.path.dirname(__file__), '../views', 'createClass.html')
        self.response.out.write(template.render(path, {'url': self.request.url, 'err': err}))


class viewSession(webapp2.RequestHandler):
    """
    Handles request to view class session details '/class/session/view'
    """

    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect("/login")
        session = ClassSession.get(self.request.get("s_key"))
        path = os.path.join(os.path.dirname(__file__), '../views', 'sessionDetails.html')
        self.response.out.write(template.render(path, {'classSession': session}))


class deleteSession(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect("/login")

        classGroup = ClassGroup.get(self.request.get("key"))
        classSession = ClassSession.get(self.request.get("s_key"))
        classGroup.classes.remove(classSession.key())
        classGroup.put()
        classSession.delete()
        self.redirect("/class/details?key=" + self.request.get("key"))


class updateSession(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
        else:
            key = self.request.get("key")
            session = ClassSession.get(self.request.get("s_key"))
            path = os.path.join(os.path.dirname(__file__), '../views', 'updateSession.html')
            self.response.out.write(template.render(path, {'key': key, 'session': session}))

    def post(self):
        """
        Checks inputs and creates a new class table
        """
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
        else:
            #try and add the class to the database else show
            #where the error has occurred
            startDate = None
            endDate = None
            startTime = None
            endTime = None

            # classid=self.request.get('cname')
            err = ""
            c_err = 0

            #check if unique id is present or not
            # if not self.checkUniqueClassID(classid):
            #     err+="\nClass Name already present.It has to be unique"
            #     c_err+=1

            #check if date is parsible or not
            try:
                startDate = datetime.strptime(self.request.get('sdate'), '%d-%m-%Y').date()

            except:
                err += "\nEnter a valid start date.Date should be ahead of today"
                c_err += 1

            #check if date is parsible or not
            try:
                endDate = datetime.strptime(self.request.get('edate'), '%d-%m-%Y').date()
            except:
                err += "\nEnter a valid end date.End date should be ahead of start date"
                c_err += 1

            #check if time is parsible or not
            try:
                startTime = datetime.strptime(self.request.get('sHrs'), '%H:%M').time()
            except:
                err += "\nEnter a valid time in startTime.Format 24Hrs"

            #check if end time is parsible or not
            try:
                endTime = datetime.strptime(self.request.get('eHrs'), '%H:%M').time()
            except:
                err += "\nEnter a valid ending time.Time format is 24Hrs"
                c_err += 1

            if c_err == 0:
            #check if can be put in database
                classGroup = ClassGroup.get(self.request.get("key"))

                # cl=l(class_id=classid,title=self.request.get('title'))
                cl = ClassSession(self.request.get("s_key"))
                cl.title = self.request.get('title')
                cl.start_date = startDate
                cl.end_date = endDate
                cl.start_time = startTime
                cl.end_time = endTime
                cl.put()

                classGroup.classes.append(cl.key())
                classGroup.put()
                self.redirect("/class/details?key=" + self.request.get("key"))
            else:
                self.redirect_with_errors(err)

    # def checkUniqueClassID(self,classid):
    #     class_present = db.GqlQuery("SELECT * FROM Class WHERE class_id = :1", classid)
    #     if class_present.count() > 0:
    #         return False
    #     return True

    def redirect_with_errors(self, err):
        path = os.path.join(os.path.dirname(__file__), '../views', 'createClass.html')
        self.response.out.write(template.render(path, {'url': self.request.url, 'err': err}))






