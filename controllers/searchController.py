__author__ = 'flingo, susi'

import webapp2
from models.models import *
from courseController import CourseUtils


class searchController(webapp2.RequestHandler):
    def get(self):
        category = self.request.get("category")
        keywords = self.request.get("keywords")
        page = self.request.get("page")
        innerHTML = self.getInnerHTML(category, keywords, page)
        self.response.out.write(innerHTML)

    def getInnerHTML(self, category, keywords, page):
        page = int(page) * 10
        innerHTML = ""
        template = "<div class='course_taken'>\
                            <table class='table table-condensed'> \
                                    <tr>\
                                        <td>\
                                            <img src='/img/cicon.png' class='image-responsive' width='40' height='40' style='border: 2px solid gray;'/>\
                                        </td>\
                                        <td>\
                                            <div class='pull-left cdetails' >\
                                                <div class='course_name'>{}</div>\
                                            </div>\
                                        </td>\
                                        <td >\
                                            <div class='pull-right _button' style='width:40px;height: 40px;'>\
                                                <a href='{}'><img src='/img/goto.png'/></a>\
                                            </div>\
                                        </td>\
                                    </tr>\
                            </table>\
                    </div>"

        if len(keywords) <= 0 or len(keywords) > 50:
            return ""

        keyset = set(self.removeDuplicates(str(keywords).strip().lower()).split())
        query = db.GqlQuery("SELECT * FROM Course WHERE category = :1", db.Key(str(category)))

        matches = []
        for result in query.run():
            search_string1 = self.removeDuplicates(str(result.course_title).strip().lower())
            search_string2 = self.removeDuplicates(str(result.desc).strip().lower())
            #search_string3 = search_string1 + search_string2
            #set2 = set(search_string3.split())
            #res = keyset.intersection(set2)
            count = 0
            for word in keyset:
                if search_string1.find(word) != -1:
                    count += 10
                if search_string2.find(word) != -1:
                    count += 1
            if count > 0:
                course_likes = CourseUtils.getCourseLikes(str(result.key())) + 1
                matches.append((count * course_likes, result.course_title, str(result.key()), course_likes))

        matches = sorted(matches, key=lambda sort_key: sort_key[0], reverse=True)

        i = j = 0
        for match in matches:
            i += 1
            if i > int(page):
                innerHTML += template.format(match[1], '/course/goto?key=' + match[2])
                j += 1
                if j >= 10:
                    break

        return innerHTML

    def removeDuplicates(self, inp):
        l = inp.split()
        ulist = []
        [ulist.append(x) for x in l if x not in ulist]
        return ' '.join(ulist)
