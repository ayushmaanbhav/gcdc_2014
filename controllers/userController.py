__author__ = 'arun'

import webapp2
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.ext import db
from courseController import CourseUtils
from models.models import *
import country
from hashlib import *
import os, re


class startPageController(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user() is not None and self.isPresent(users.get_current_user()):
            self.redirect('/profile')
        else:
            path = os.path.join(os.path.dirname(__file__), '../views', 'index.html')
            self.response.out.write(template.render(path, {'greeting': '/google/login'}))

    def isPresent(self, user):
        t = db.GqlQuery("SELECT * FROM User where user_id = :1", user.user_id()).count()
        if t:
            return True
        return False


class googleLoginController(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            self.redirect('/login/check')
            return
        self.redirect(users.create_login_url(self.request.uri))


class showProfile(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            path = os.path.join(os.path.dirname(__file__), '../views', 'profile.html')
            usr = db.GqlQuery("SELECT * FROM User WHERE user_id = :1", users.get_current_user().user_id())
            usr = usr.get()
            query = db.GqlQuery("SELECT * FROM Course")
            courses = []
            for course in query.run(limit=5):
                courses.append((course, CourseUtils.getCourseLikes(course.key())))
            self.response.out.write(template.render(path, {'user': usr, 'courses': courses}))
        else:
            self.redirect('/login')


class loginRedirectStation(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user() is None:
            self.redirect('/')
        elif self.isPresent(users.get_current_user()):
            self.redirect('/profile')
        else:
            self.redirect('/login/create')

    def isPresent(self, user):
        t = db.GqlQuery("SELECT * FROM User where user_id = :1", user.user_id()).count()
        if t:
            return True
        return False


class myCourses(webapp2.RedirectHandler):
    def get(self):
        if users.get_current_user():
            path = os.path.join(os.path.dirname(__file__), '../views', 'mycourses.html')
            self.response.out.write(template.render(path, {}))
        else:
            self.redirect('/login')

    def getInnerHTML(self):
        template = "<div class='course_taken'>\
                        <table class='table table-condensed'> \
                            <tr>\
                                <td>\
                                    <img src='/img/cicon.png' class='image-responsive' width='40' height='40' style='border: 2px solid gray;'/>\
                                </td>\
                                 <td>\
                                                    <div class='pull-left cdetails' >\
                                                        <div class='course_name'>{}</div>\
                                                    </div>\
                                                </td>\
                                                <td >\
                                                    <div class='pull-right _button' style='width:40px;height: 40px;'>\
                                                        <a href='{}'><img src='/img/goto.png'/></a>\
                                                    </div>\
                                                </td>\
                                            </tr>\
                                        </table>\
                                    </div>"
        return template


class myClasses(webapp2.RedirectHandler):
    def get(self):
        if users.get_current_user():
            path = os.path.join(os.path.dirname(__file__), '../views', 'myclasses.html')
            self.response.out.write(template.render(path, {'greeting': users.create_login_url('/')}))
        else:
            self.redirect('/login')

    def getInnerHTML(self):
        template = "<div class='course_taken'>\
                                    <table class='table table-condensed'> \
                                            <tr>\
                                                <td>\
                                                    <img src='/img/cicon.png' class='image-responsive' width='40' height='40' style='border: 2px solid gray;'/>\
                                                </td>\
                                                <td>\
                                                    <div class='pull-left cdetails' >\
                                                        <div class='course_name'>{}</div>\
                                                    </div>\
                                                </td>\
                                                <td >\
                                                    <div class='pull-right _button' style='width:40px;height: 40px;'>\
                                                        <a href='{}'><img src='/img/goto.png'/></a>\
                                                    </div>\
                                                </td>\
                                            </tr>\
                                        </table>\
                                    </div>"
        return template


class search(webapp2.RedirectHandler):
    def get(self):
        if users.get_current_user():
            path = os.path.join(os.path.dirname(__file__), '../views', 'search.html')
            categories = Category.all()
            self.response.out.write(template.render(path, {'categories': categories}))
        else:
            self.redirect('/login')


class CreateUserController(webapp2.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), '../views', 'createuser.html')
        self.response.out.write(
            template.render(path, {'greeting': users.create_login_url('/'), 'countries': country.countries}))


class CreateUserSubmitForm(webapp2.RequestHandler):
    def post(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
        else:
            user_name = self.request.get('username')
            user_email = user.email()
            user_id = user.user_id()
            name = self.request.get('name')
            phone = self.request.get('phone')
            count = self.request.get('country')
            state = self.request.get('state')
            city = self.request.get('city')
            pincode = self.request.get('pincode')
            address = self.request.get('address')
            about = self.request.get('about')
            motto = self.request.get('motto')
            qualifications = self.request.get('qualifications')
            profpic = None

            if self.request.get('gravatar') == 1:
                profpic = "http://www.gravatar.com/avatar/" + md5(user.email().encode("utf")).hexdigest()
            else:
                profpic = '/img/defaultpic.png'

            err = ""
            c_err = 0


            #check if unique id is present or not
            if not self.checkUniqueUsername(user_name):
                err += "\nUsername already present. Please enter a unique username."
                self.redirect_with_errors(err)
                c_err += 1
            if not re.match('.+', name, re.M | re.I):
                err += "\nPlease enter a valid name."
                self.redirect_with_errors(err)
                c_err += 1
            if not re.match('\d{10}', phone, re.M | re.I):
                err += "\nPlease enter a valid contact number of 10 digits."
                self.redirect_with_errors(err)
                c_err += 1
            if not re.match('.+', city, re.M | re.I):
                err += "\nPlease enter a valid city name."
                self.redirect_with_errors(err)
                c_err += 1
            if not re.match('\d+', pincode, re.M | re.I):
                err += "\nPlease enter a valid pincode."
                self.redirect_with_errors(err)
                c_err += 1
            if not re.match('.+', qualifications, re.M | re.I):
                err += "\nPlease enter your qualifications."
                self.redirect_with_errors(err)
                c_err += 1

            print c_err
            if not c_err:
            #check if can be put in database
                try:
                    ur = User(user_id=user_id, email=user_email, username=user_name, name=name, phone=phone,
                              country=count, city=city, pincode=int(pincode), qualifications=qualifications,
                              profilepic=profpic)
                    ur.state = state
                    ur.address = address
                    ur.about = about
                    ur.motto = motto
                    ur.put()
                    self.redirect('/profile')
                except Exception as e:
                    print str(e)
                    self.redirect_with_errors(err)
            else:
                self.redirect_with_errors(err)

    def checkUniqueUsername(self, uid):
        class_present = db.GqlQuery("SELECT * FROM User WHERE username = :1", uid)
        count = 0
        for i in class_present.run(limit=5):
            count += 1
        if not count == 0:
            return False
        return True

    def redirect_with_errors(self, err):
        path = os.path.join(os.path.dirname(__file__), '../views', 'createUser.html')
        self.response.out.write(
            template.render(path, {'url': self.request.url, 'err': err, 'countries': country.countries}))
