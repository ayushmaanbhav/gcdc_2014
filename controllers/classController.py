__author__ = 'saswat'
import webapp2
from google.appengine.api import users
from google.appengine.ext.webapp import template
from models.models import *
from google.appengine.ext import db
import os
from datetime import datetime

#class classController(webapp2.RequestHandler):
#    def get(self):
#        #check if user is logged in
#        user='Saswat'
#        #user=users.get_current_user()
#        #if user is not logged in ask the user to log in and continue
#        if not user:
#            #TODO create a login page that after login will redirect back to the same page
#            self.redirect('/login')
#        else:
#            # get user details from datastore
#            # userObj=db.GqlQuery("SELECT * FROM ")
#            # for create the url would be /class/create
#            # for view the url would be /class/goto?=classid
#            if self.request.url.find('/class/create')>=0:
#                path=os.path.join(os.path.dirname(__file__),'../views','createClass.html')
#                self.response.out.write(template.render(path,{'url':self.request.url}))
#            elif self.request.url.find('/class/submit')>=0:
#                self.addClass()
#                path=os.path.join(os.path.dirname(__file__),'../views','myclass.html')
#                self.response.out.write(template.render(path,{'url':self.request.url}))
#            else:
#                path=os.path.join(os.path.dirname(__file__),'../views','myclass.html')
#                self.response.out.write(template.render(path,{'url':self.request.url}))
#
#    def post(self):
#        user='Saswat'
#        if not user:
#            self.redirect('/login')
#        else:
#            self.addClass()
#            text=""
#            text+=" :|: "+self.request.get('cname')
#            text+=" :|: "+self.request.get('title')
#            self.response.out.write("Success")
#
#
#    def addClass(self):
#        cls=Class(class_id=self.request.get('cname'),title=self.request.get('title'))
#        #cls.category=self.request.get('category')
#        cls.start_date=datetime.strptime(self.request.get('sdate'),'%d-%m-%Y').date()
#        cls.end_date=datetime.strptime(self.request.get('edate'),'%d-%m-%Y').date()
#        cls.start_time=datetime.strptime(self.request.get('sHrs'), '%H:%M').time()
#        cls.end_time=datetime.strptime(self.request.get('eHrs'), '%H:%M').time()
#        cls.put()

class classLogin(webapp2.RequestHandler):
    """
    Handles login to '/class' url handle
    """

    def get(self):
        user = "Saswat"
        #user = users.get_current_user()
        if not user:
            self.redirect('/login')
        else:
            path = os.path.join(os.path.dirname(__file__), '../views', 'gotoCourse.html')
            self.response.out.write(template.render(path, {'url': self.request.url}))


class createForm(webapp2.RequestHandler):
    """
    Handles creating a form in '/class/create' url
    """

    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
        else:
            path = os.path.join(os.path.dirname(__file__), '../views', 'createClass.html')
            self.response.out.write(template.render(path, {'url': self.request.url, 'categories': Category.all()}))


class submitForm(webapp2.RequestHandler):
    """
    Handles submitting the form in the '/class/create/submit'
    """

    def post(self):
        """
        Checks inputs and creates a new class table
        """
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
        else:
            #try and add the class to the database else show
            #where the error has occurred
            startDate = None
            endDate = None
            startTime = None
            endTime = None

            classid = self.request.get('cname')
            err = ""
            c_err = 0

            #check if unique id is present or not
            if not self.checkUniqueClassID(classid):
                err += "\nClass Name already present.It has to be unique"
                c_err += 1

            #check if date is parsible or not
            try:
                startDate = datetime.strptime(self.request.get('sdate'), '%d-%m-%Y').date()
            except:
                err += "\nEnter a valid start date.Date should be ahead of today"
                c_err += 1

            #check if date is parsible or not
            try:
                endDate = datetime.strptime(self.request.get('edate'), '%d-%m-%Y').date()
            except:
                err += "\nEnter a valid end date.End date should be ahead of start date"
                c_err += 1

            #check if time is parsible or not
            try:
                startTime = datetime.strptime(self.request.get('sHrs'), '%H:%M').time()
            except:
                err += "\nEnter a valid time in startTime.Format 24Hrs"

            #check if end time is parsible or not
            try:
                endTime = datetime.strptime(self.request.get('eHrs'), '%H:%M').time()
            except:
                err += "\nEnter a valid ending time.Time format is 24Hrs"
                c_err += 1

            if not c_err:
            #check if can be put in database
                try:
                    cl = Class(class_id=classid, title=self.request.get('title'))
                    cl.start_date = startDate
                    cl.category = self.getCategoryRef(self.request.get('category'))
                    #print(cl.key())
                    cl.end_date = endDate
                    cl.start_time = startTime
                    cl.end_time = endTime
                    cl.put()
                    self.response.out.write("Success")
                except Exception as e:
                    #err+= str(e)
                    self.redirect_with_errors(err)
            else:
                self.redirect_with_errors(err)

    def getCategoryRef(self, category):
        """
        Selects and returns the category reference
        """
        rval = None
        cat = db.GqlQuery("SELECT * FROM Category WHERE category_id = :1", category)
        for c in cat.run():
            rval = c
            break
        return rval.key()

    def checkUniqueClassID(self, classid):
        class_present = db.GqlQuery("SELECT * FROM Class WHERE class_id = :1", classid)
        if class_present.count() > 0:
            return False
        return True

    def redirect_with_errors(self, err):
        path = os.path.join(os.path.dirname(__file__), '../views', 'createClass.html')
        self.response.out.write(
            template.render(path, {'url': self.request.url, 'err': err, 'categories': Category.all()}))


class viewClassList(webapp2.RequestHandler):
    """
    Handles request to list all classes '/class/list'
    """

    def get(self):
        #TODO fetch only a limited amount
        path = os.path.join(os.path.dirname(__file__), '../views', 'listOfClass.html')
        self.response.out.write(template.render(path, {'classes': Class.all()}))


class goToClass(webapp2.RequestHandler):
    """
    Handles going to class room in '/class/view?=class_key
    """

    def get(self):
        pass


class enrollInClass(webapp2.RequestHandler):
    """
    Handles registering for a class for a logged in user
    '/class/enroll?=class_key'
    """

    def get(self):
        key = self.request.get('key')
        cl = None
        try:
            cl = db.get(keys=key)
        except Exception as e:
            path = os.path.join(os.path.dirname(__file__), '../views', '404.html')
            self.response.out.write(template.render(path, {'class': cl}))

        if self.isAlreadyEnrolled(cl):
            self.response.out.write("Is already enrolled")
        else:
            user = users.get_current_user()
            if not user:
                self.redirect('/login')
            else:
                try:
                    reg = classTaken()
                    reg.class_key = cl
                    #                    print(cl.key())
                    #                    print(user.user_id())
                    reg.user_key = user
                    reg.save()
                    path = os.path.join(os.path.dirname(__file__), '../views', 'viewClass.html')
                    self.response.out.write(template.render(path, {'class': cl}))
                except Exception as e:
                    print(str(e))

    def isAlreadyEnrolled(self, cl):
        query = db.GqlQuery("SELECT * FROM enrolledUsers WHERE user_key = :1 AND class_key = :2",
                            users.get_current_user(), cl)
        if query.count() > 0:
            return True
        return False