import webapp2
from google.appengine.api import users
from google.appengine.ext.webapp import template
import os
from models.models import *
from google.appengine.ext import db


class editLink(webapp2.RedirectHandler):
    """
        never used
    """

    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            # course = Course.get(self.request.get('key'))
            link = CourseLinks.get(self.request.get('linkkey'))
            path = os.path.join(os.path.dirname(__file__), '../views', 'updateLink.html')
            self.response.write(template.render(path, {'course_key': self.request.get('key'), 'link': link}))

    def post(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            c_err = 0
            err = ""
            title = None
            link = None
            linkkey = None
            key = None
            try:
                title = self.request.get("title")
            except:
                c_err += 1
                err += "Invalid title"
            try:
                link = self.request.get("link")
            except:
                c_err += 1
                err += "Invalid link"
            try:
                linkkey = self.request.get("linkkey")
            except:
                c_err += 1
                err += "Invalid key"
            try:
                key = self.request.get("key")
            except:
                c_err += 1
                err += "Invalid key"

            if c_err == 0:
                courseLink = CourseLinks.get(linkkey)
                courseLink.link = link
                courseLink.title = title
                courseLink.put()

                self.redirect('/course/details?key=' + self.request.get('key'))
            else:
                self.redirect_with_error(err)

    def redirect_with_error(self, err):
        self.redirect('/course/details?key=' + self.request.get('key'))


class deleteLink(webapp2.RedirectHandler):
    def post(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            lkey = self.request.get('lkey')
            ckey = self.request.get('ckey')
            print lkey
            print ckey
            course = Course.get(ckey)
            course.links.remove(CourseLinks.get(lkey).key())
            course.put()
            courseLinks = CourseLinks.get(lkey)
            courseLinks.delete()

            self.redirect('/course/links?key=' + str(ckey))


class showCourseUpdate(webapp2.RedirectHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            course = Course.get(self.request.get('key'))
            links = CourseLinks.get(course.links)
            categories = Category.all()

            course_owners = []
            crse = CourseTaught.gql("WHERE course = :1", db.Key(str(course.key()))).fetch(limit=None)
            for cr in crse:
                course_owners.append(cr.user.username)
                #self.response.out.write(cr.user.username)

            path = os.path.join(os.path.dirname(__file__), '../views', 'updateCourse.html')
            self.response.write(template.render(path, {'owners': course_owners, 'course': course, 'links': links,
                                                       'categories': categories}))


class updateCourse(webapp2.RedirectHandler):
    def post(self):

        user = users.get_current_user()
        key = self.request.get("key")
        if not user:
            self.redirect('/login')
            return
        elif not key:
            self.redirect('/')
            return
        else:
            c_err = 0
            err = ""
            links = []
            count = None
            course_name = None
            category = None
            description = None
            drive_link = None
            group_link = None

            try:
                course_name = self.request.get("course_name")
            except:
                c_err += 1
                err += "Invalid Title"

            try:
                category = self.request.get("course_category")
            except:
                c_err += 1
                err += "Invalid Category"

            try:
                description = self.request.get("description")
            except:
                c_err += 1
                err += "Enter a description for the viewers"

            try:
                drive_link = self.request.get("gdrive")
            except:
                c_err += 1
                err += "Invalid URL"

            try:
                group_link = self.request.get("discuss")
            except:
                c_err += 1
                err += "Invalid URL"

            if c_err == 0:
                course = Course.get(key)
                course.course_title = course_name
                course.category = Category.get(category)
                course.desc = description
                course.gdrive = drive_link
                course.forum = group_link
                course.put()

                crse = CourseTaught.gql("WHERE course = :1", db.Key(key)).fetch(limit=None)
                for cr in crse:
                    cr.delete()

                course_owners = CourseTaught()
                course_owners.course = course
                course_owners.user = User.gql("WHERE user_id = :1", user.user_id()).get()
                course_owners.put()
                owners_string = self.request.get("owners").strip()
                if owners_string is not None and owners_string != "":
                    owners = owners_string.split(',')
                    for owner in owners:
                        usr = User.gql("WHERE username = :1", owner).get()
                        if usr:
                            course_owner = CourseTaught()
                            course_owner.course = course
                            course_owner.user = usr
                            course_owner.put()

                self.redirect('/course/links?key=' + str(course.key()))
            else:
                self.redirect_with_error(err)
                return

    def redirect_with_error(self, err):
        course = Course.get(self.request.get('key'))
        links = CourseLinks.get(course.links)
        categories = Category.all()
        path = os.path.join(os.path.dirname(__file__), '../views', 'updateCourse.html')
        self.response.out.write(
            template.render(path, {'course': course, 'links': links, 'categories': categories, 'err': err}))


class viewAllCourses(webapp2.RequestHandler):
    """
        never used
    """
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:

            courses = Course.all()

            path = os.path.join(os.path.dirname(__file__), '../views', 'viewCourses.html')
            self.response.write(template.render(path, {'courses': courses}))


class viewCourse(webapp2.RedirectHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            c_key = self.request.get('key')
            course = Course.get(c_key)
            links = []
            for key in course.links:
                course_link = CourseLinks.get(key)
                links.append(course_link)
            obj1 = CourseUtils.getCourseLikes(c_key)
            obj2 = CourseUtils.getCourseTaken(c_key)
            obj3 = CourseUtils.getCourseTaught(c_key)
            path = os.path.join(os.path.dirname(__file__), '../views', 'gotoCourse.html')
            self.response.write(template.render(path, {'course': course, 'links': links, 'likes': obj1, 'attend': obj2,
                                                       'taught': obj3}))


class showCourseCreate(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            path = os.path.join(os.path.dirname(__file__), '../views', 'startCourse.html')
            categories = Category.all()
            self.response.out.write(template.render(path, {'categories': categories}))


class showAddLinksPage(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            path = os.path.join(os.path.dirname(__file__), '../views', 'addLink.html')
            course = None
            try:
                course = Course.get(self.request.get('key'))
            except:
                self.redirect('/course/create')
            links = []
            for key in course.links:
                course_link = CourseLinks.get(key)
                links.append(course_link)
            self.response.out.write(template.render(path, {'links': links, 'key': str(course.key())}))


class addLink(webapp2.RequestHandler):
    def post(self):

        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            key = self.request.get('key')
            course = Course.get(key)
            vtitle = self.request.get('vtitle')
            lname = self.request.get('lname')
            course_link = CourseLinks(link=lname, title=vtitle)
            course_link.put()
            course.links.append(course_link.key())
            course.put()
            self.redirect('/course/links?key=' + str(key))


class createCourse(webapp2.RequestHandler):
    def post(self):

        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            c_err = 0
            err = ""
            links = []
            count = None
            course_name = None
            category = None
            description = None
            drive_link = None
            group_link = None

            try:
                course_name = self.request.get("course_name")
            except:
                c_err += 1
                err += "Invalid Title"

            try:
                category = self.request.get("course_category")
            except:
                c_err += 1
                err += "Invalid Category"

            try:
                description = self.request.get("description")
            except:
                c_err += 1
                err += "Enter a description for the viewers"

            try:
                drive_link = self.request.get("gdrive")
            except:
                c_err += 1
                err += "Invalid URL"

            try:
                group_link = self.request.get("discuss")
            except:
                c_err += 1
                err += "Invalid URL"

            if c_err == 0:
                course = Course(course_title=course_name)
                course.category = Category.get(category)
                course.desc = description
                course.gdrive = drive_link
                course.forum = group_link
                course.links = []
                course.put()

                course_owners = CourseTaught()
                course_owners.course = course
                course_owners.user = User.gql("WHERE user_id = :1", user.user_id()).get()
                course_owners.put()
                owners_string = self.request.get("owners").strip()
                if owners_string is not None and owners_string != "":
                    owners = owners_string.split(',')
                    for owner in owners:
                        usr = User.gql("WHERE username = :1", owner).get()
                        if usr:
                            course_owner = CourseTaught()
                            course_owner.course = course
                            course_owner.user = usr
                            course_owner.put()

                self.redirect('/course/links?key=' + str(course.key()))
            else:
                self.redirect_with_error(err)
                return

    def redirect_with_error(self, err):
        categories = Category.all()
        path = os.path.join(os.path.dirname(__file__), '../views', 'startCourse.html')
        self.response.out.write(template.render(path, {'categories': categories, 'err': err}))


class courseAdded(webapp2.RequestHandler):
    def post(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            key = self.request.get('key')
            self.redirect('/course/goto?key=' + str(key))


class enrollInCourse(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            key = self.request.get('key')
            usr = User.gql("WHERE user_id = :1", user.user_id()).get()
            if CourseTaken.gql("WHERE user = :1 AND course = :2", db.Key(str(usr.key())), db.Key(key)).get() is None:
                course_taken = CourseTaken()
                course_taken.course = Course.get(key)
                course_taken.user = usr
                course_taken.put()
            self.response.out.write("SUCCESS")


class leaveCourse(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            key = self.request.get('key')
            usr = User.gql("WHERE user_id = :1", user.user_id()).get()
            ct = CourseTaken.gql("WHERE user = :1 AND course = :2", db.Key(str(usr.key())), db.Key(key)).get()
            if ct is not None:
                ct.delete()
            self.response.out.write("SUCCESS")


class submitFeedback(webapp2.RequestHandler):
    """
        we can submit feedback through google forms !!!
    """
    def post(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            key = self.request.get('key')
            usr = User.gql("WHERE user_id = :1", user.user_id()).get()
            course_fdbk = CourseFeedBack()
            course_fdbk.course = Course.get(key)
            course_fdbk.user = usr
            course_fdbk.feedback = self.request.get('feedback')
            course_fdbk.put()
            self.response.out.write("SUCCESS")


class likeCourse(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            key = self.request.get('key')
            usr = User.gql("WHERE user_id = :1", user.user_id()).get()
            if CourseLikes.gql("WHERE user = :1 AND course = :2", db.Key(str(usr.key())), db.Key(key)).get() is None:
                course_l = CourseLikes()
                course_l.course = Course.get(key)
                course_l.user = usr
                course_l.put()
            self.response.out.write("SUCCESS")


class unlikeCourse(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect('/login')
            return
        else:
            key = self.request.get('key')
            usr = User.gql("WHERE user_id = :1", user.user_id()).get()
            ct = CourseLikes.gql("WHERE user = :1 AND course = :2", db.Key(str(usr.key())), db.Key(key)).get()
            if ct is not None:
                ct.delete()
            self.response.out.write("SUCCESS")


class CourseUtils:
    def __init__(self):
        pass

    @staticmethod
    def getCourseLikes(key):
        return CourseLikes.gql("WHERE course = :1", db.Key(str(key))).count()

    @staticmethod
    def getCourseTaken(key):
        return CourseTaken.gql("WHERE course = :1", db.Key(str(key))).count()

    @staticmethod
    def getCourseTaught(key):
        return CourseTaught.gql("WHERE course = :1", db.Key(str(key))).count()

