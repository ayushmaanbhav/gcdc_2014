__author__ = 'Saswat'
# File used to generate test date for datastore
from models.models import *
from google.appengine.ext.webapp import template
import webapp2
import os


class GenerateTestData(webapp2.RequestHandler):
    def get(self):
        self.generateCategories()
        self.response.out.write("Finished")

    def generateCategories(self):
        c1 = Category(category_id="15J4", category_name="ECE", description="About cs")
        c1.put()
        c2 = Category(category_id="15L4", category_name="Hobbies", description="About cs")
        c2.put()
        c3 = Category(category_id="15J4", category_name="Medicine", description="About cs")
        c3.put()


class pickerHandler(webapp2.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), '../views', 'picker.html')
        self.response.out.write(template.render(path, {}))


class hangoutHandler(webapp2.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), '../views', 'hangout.html')
        self.response.out.write(template.render(path, {}))


class hangoutStart(webapp2.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), '../views', 'start.html')
        self.response.out.write(template.render(path, {}))


class ContactUs(webapp2.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), '../views', 'contacus.html')
        self.response.out.write(template.render(path, {}))


